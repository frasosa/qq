package qqdb

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/lib/pq"
)

var err error

// connects to the database deployed on neon
// neon.tech
func connect() *sql.DB {
	connStr := os.Getenv("DATABASE")
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Panic(err)
	}
	return db
}

// initalizes database on api startup
// creating tables if needed
func Init() {
	db := connect()

	// create users table
	_, err := db.Exec(`
        CREATE TABLE IF NOT EXISTS users (
		id SERIAL PRIMARY KEY,
		email VARCHAR(255) NOT NULL,
		username VARCHAR(100) NOT NULL,
		password VARCHAR(255) NOT NULL,
		phone VARCHAR(15),
		picture BYTEA,
        UNIQUE (email),
        UNIQUE (username)
	);`)
	if err != nil {
		log.Panic("Error creating Users table: ", err)
	}

	// create queues table
	_, err = db.Exec(`
        CREATE TABLE IF NOT EXISTS Queues (
        id SERIAL PRIMARY KEY,
        owner INT NOT NULL REFERENCES Users(id),
        name VARCHAR(255) NOT NULL,
        size INT NOT NULL 
        CONSTRAINT positive_size CHECK (size >= 0)
    );
    `)
	if err != nil {
		log.Panic("Error creating Queues table: ", err)
	}

	// create queue memberships table
	_, err = db.Exec(`
        CREATE TABLE IF NOT EXISTS Queue_Memberships (
        id SERIAL PRIMARY KEY,
        queue_id INT NOT NULL REFERENCES Queues(id),
        user_id INT NOT NULL REFERENCES Users(id),
        position INT NOT NULL,
        UNIQUE (queue_id, user_id),
        CONSTRAINT positive_position CHECK (position > 0)
  );
`)
	if err != nil {
		log.Panic("Error creating Queue_Memberships table: ", err)
	}

	log.Println("Tables created successfully!")
}

// wrapper for actions that must be done in a transaction
func withTransaction(txFunc func(*sql.Tx) (interface{}, *DbError)) (interface{}, *DbError) {
	db := connect()
	defer db.Close()

	tx, err := db.Begin()
	if err != nil {
		return nil, NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to begin transaction: %v", err))
	}

	result, dbErr := txFunc(tx)
	if dbErr == nil {
		err = tx.Commit()
		if err != nil {
			return nil, NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to commit transaction: %v", err))
		}
	}

	return result, dbErr
}

// attempts to rollback a transaction on an error
func rollbackTransaction(tx *sql.Tx) {
	if e := tx.Rollback(); e != nil {
		log.Panic(e)
	}
}

// adds user to database
func CreateUser(user User) (int, *DbError) {
	db := connect()
	defer db.Close()
	var id int

	// insert new user
	err := db.QueryRow("INSERT INTO Users(email, username, password) VALUES($1, $2, $3) RETURNING id",
		user.Email, user.Username, user.Password).Scan(&id)
	if err != nil {
		pgErr, ok := err.(*pq.Error)
		if ok {
			if pgErr.Code == "23505" { // unique violation
				if pgErr.Constraint == "users_email_key" {
					return 0, NewDbError(http.StatusBadRequest, "That email already has an account associated with it")
				} else if pgErr.Constraint == "users_username_key" {
					return 0, NewDbError(http.StatusBadRequest, "Username already in use")
				}
			}
		}
		return 0, NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to create user: %v", err))
	}

	return id, nil
}

// finds a user by their email
func LookupUserByEmail(email string) (User, *DbError) {
	db := connect()
	defer db.Close()
	var user User

	// get user with this email
	err := db.QueryRow("SELECT id, email, username, password FROM users WHERE email = $1", email).Scan(&user.ID, &user.Email, &user.Username, &user.Password)
	if err != nil {
		if err == sql.ErrNoRows {
			return User{}, NewDbError(http.StatusNotFound, "there is no account with this email")
		}
		return User{}, NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to lookup user: %v", err))
	}

	return user, nil
}

// gets all the queues from the database
func GetAllQueues() ([]DiskQueue, *DbError) {
	db := connect()
	defer db.Close()

	rows, err := db.Query("SELECT id, owner, name, size FROM Queues")
	if err != nil {
		return nil, NewDbError(http.StatusInternalServerError, fmt.Sprintf("error querying queues: %v", err))
	}
	defer rows.Close()

	var queues []DiskQueue
	for rows.Next() {
		var queue DiskQueue
		if err := rows.Scan(&queue.ID, &queue.Owner, &queue.Name, &queue.Size); err != nil {
			return nil, NewDbError(http.StatusInternalServerError, fmt.Sprintf("error scanning queue row: %v", err))
		}
		queues = append(queues, queue)
	}

	if err = rows.Err(); err != nil {
		return nil, NewDbError(http.StatusInternalServerError, fmt.Sprintf("error iterating rows: %v", err))
	}

	return queues, nil
}

// gets a specific queue including its members
// within a transaction
func GetQueue(id int) (MemQueue, *DbError) {
	result, dbErr := withTransaction(func(tx *sql.Tx) (interface{}, *DbError) {
		return getQueue(tx, id)
	})
	return result.(MemQueue), dbErr
}

// gets a specific queue including its members
// SHOULD BE CALLED WITHIN A TRANSACTION
func getQueue(tx *sql.Tx, id int) (MemQueue, *DbError) {
	var memQueue MemQueue

	// query the queue
	err := tx.QueryRow("SELECT id, owner, name FROM Queues WHERE id = $1", id).Scan(&memQueue.ID, &memQueue.Owner, &memQueue.Name)
	if err != nil {
		defer rollbackTransaction(tx)
		if err == sql.ErrNoRows {
			return MemQueue{}, NewDbError(http.StatusNotFound, "queue not found")
		}
		return MemQueue{}, NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to query queue: %v", err))
	}

	// query the queue memberships
	rows, err := tx.Query(`
		SELECT u.id, u.email, u.username
		FROM Queue_Memberships qm
		JOIN Users u ON qm.user_id = u.id
		WHERE qm.queue_id = $1
		ORDER BY qm.position;
	`, id)
	if err != nil {
		defer rollbackTransaction(tx)
		return MemQueue{}, NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to query queue memberships: %v", err))
	}
	defer rows.Close()

	// scan the result into users slice
	var users []User
	for rows.Next() {
		var user User
		if err := rows.Scan(&user.ID, &user.Email, &user.Username); err != nil {
			return MemQueue{}, NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to scan queue membership: %v", err))
		}
		users = append(users, user)
	}
	if err = rows.Err(); err != nil {
		return MemQueue{}, NewDbError(http.StatusInternalServerError, fmt.Sprintf("error iterating queue memberships: %v", err))
	}

	memQueue.Users = users
	return memQueue, nil
}

// deletes a user, their queues, and their queue memberships
func DeleteUser(id int) *DbError {
	_, dbErr := withTransaction(func(tx *sql.Tx) (interface{}, *DbError) {
		// find all the queues this user is a member of
		wQueues, dbErr := waitingQueues(tx, id)
		if dbErr != nil {
			return nil, dbErr
		}

		// remove the user from all the queues
		for _, q := range wQueues {
			dbErr = removeUserFromQueue(tx, id, q.ID)
			if dbErr != nil {
				return nil, dbErr
			}
		}

		// find all the queues this user owns
		oQueues, dbErr := ownedQueues(tx, id)
		if dbErr != nil {
			return nil, dbErr
		}

		// delete owned queues
		for _, q := range oQueues {
			dbErr = deleteQueue(tx, q.ID)
			if dbErr != nil {
				return nil, dbErr
			}
		}

		// delete user
		_, err = tx.Exec("DELETE FROM Users WHERE id = $1", id)
		if err != nil {
			defer rollbackTransaction(tx)
			return nil, NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to delete user: %v", err))
		}

		return nil, nil
	})
	return dbErr
}

// creates a new queue
func CreateQueue(queue DiskQueue) (int, *DbError) {
	db := connect()
	defer db.Close()

	var id int

	// insert the new queue
	err := db.QueryRow("INSERT INTO Queues(owner, name, size) VALUES($1, $2, $3) RETURNING id",
		queue.Owner, queue.Name, queue.Size).Scan(&id)
	if err != nil {
		return 0, NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to create queue: %v", err))
	}

	return id, nil
}

// deletes a new queue and all the memberships associated with it
// within a transaction
func DeleteQueue(id int) *DbError {
	_, dbErr := withTransaction(func(tx *sql.Tx) (interface{}, *DbError) {
		return nil, deleteQueue(tx, id)
	})

	return dbErr
}

// deletes a new queue and all the memberships associated with it
// SHOULD BE CALLED WITHIN A TRANSACTION
func deleteQueue(tx *sql.Tx, id int) *DbError {

	// delete queue memberships associated with the queue
	_, err = tx.Exec("DELETE FROM Queue_Memberships WHERE queue_id = $1", id)
	if err != nil {
		defer rollbackTransaction(tx)
		return NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to delete queue memberships: %v", err))
	}

	// delete the queue itself
	_, err = tx.Exec("DELETE FROM Queues WHERE id = $1", id)
	if err != nil {
		defer rollbackTransaction(tx)
		return NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to delete queue: %v", err))
	}

	return nil
}

// gets all a users owned queues within a transaction
func WaitingQueues(id int) ([]PosQueue, *DbError) {
	result, dbErr := withTransaction(func(tx *sql.Tx) (interface{}, *DbError) {
		return waitingQueues(tx, id)
	})
	return result.([]PosQueue), dbErr
}

// gets all the queues a user is in
// SHOULD BE CALLED WITHIN A TRANSACTION
func waitingQueues(tx *sql.Tx, id int) ([]PosQueue, *DbError) {
	// Query all queues where the user is a member
	rows, err := tx.Query(`
        SELECT q.id, q.owner, q.name, q.size, qm.position
        FROM Queues q
        JOIN Queue_Memberships qm ON q.id = qm.queue_id
        WHERE qm.user_id = $1
    `, id)
	if err != nil {
		defer rollbackTransaction(tx)
		return nil, NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to find user's memberships: %v", err))
	}
	defer rows.Close()

	var queues []PosQueue
	for rows.Next() {
		var queue PosQueue
		if err := rows.Scan(&queue.ID, &queue.Owner, &queue.Name, &queue.Size, &queue.Position); err != nil {
			return nil, NewDbError(http.StatusInternalServerError, fmt.Sprintf("error scanning queue row: %v", err))
		}
		queues = append(queues, queue)
	}

	if err := rows.Err(); err != nil {
		return nil, NewDbError(http.StatusInternalServerError, fmt.Sprintf("error iterating rows: %v", err))
	}

	return queues, nil
}

// gets all a users owned queues within a transaction
func OwnedQueues(id int) ([]DiskQueue, *DbError) {
	result, dbErr := withTransaction(func(tx *sql.Tx) (interface{}, *DbError) {
		return ownedQueues(tx, id)
	})
	return result.([]DiskQueue), dbErr
}

// gets all the queues a user is in
// SHOULD BE CALLED WITHIN A TRANSACTION
func ownedQueues(tx *sql.Tx, id int) ([]DiskQueue, *DbError) {
	// find all the queues this user owns
	rows, err := tx.Query("SELECT * FROM Queues WHERE owner = $1", id)
	if err != nil {
		defer rollbackTransaction(tx)
		return nil, NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to find user's owned queues: %v", err))
	}
	defer rows.Close()

	var queues []DiskQueue
	for rows.Next() {
		var queue DiskQueue
		if err := rows.Scan(&queue.ID, &queue.Owner, &queue.Name, &queue.Size); err != nil {
			return nil, NewDbError(http.StatusInternalServerError, fmt.Sprintf("error scanning queue row: %v", err))
		}
		queues = append(queues, queue)
	}

	if err = rows.Err(); err != nil {
		return nil, NewDbError(http.StatusInternalServerError, fmt.Sprintf("error iterating rows: %v", err))
	}

	return queues, nil
}

// adds a user to a queue within a transaction
func AppendUserToQueue(userId int, queueId int) *DbError {
	_, dbErr := withTransaction(func(tx *sql.Tx) (interface{}, *DbError) {
		return nil, appendUserToQueue(tx, userId, queueId)
	})

	return dbErr
}

// adds user to queue
// SHOULD BE CALLED WITHIN A TRANSACTION
func appendUserToQueue(tx *sql.Tx, userId int, queueId int) *DbError {
	// get queue size
	var diskQueue DiskQueue
	err = tx.QueryRow("SELECT id, owner, name, size FROM Queues WHERE id = $1",
		queueId).Scan(&diskQueue.ID, &diskQueue.Owner, &diskQueue.Name, &diskQueue.Size)
	if err != nil {
		defer rollbackTransaction(tx)
		if err == sql.ErrNoRows {
			return NewDbError(http.StatusNotFound, "queue not found")
		}
		return NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to get queue: %v", err))
	}

	// make sure the user is not the owner of the queue they are trying to join
	if diskQueue.Owner == userId {
		defer rollbackTransaction(tx)
		return NewDbError(http.StatusBadRequest, ("user cannot join a queue they own"))
	}

	// insert new queue membership
	var memId int
	err = tx.QueryRow("INSERT INTO Queue_Memberships(queue_id, user_id, position) VALUES($1, $2, $3) RETURNING id",
		queueId, userId, diskQueue.Size+1).Scan(&memId)
	if err != nil {
		defer rollbackTransaction(tx)
		return NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to add user to queue: %v", err))
	}

	// update queue size
	_, err = tx.Exec("UPDATE Queues SET size = $1 WHERE id = $2", diskQueue.Size+1, diskQueue.ID)
	if err != nil {
		defer rollbackTransaction(tx)
		return NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to update queue size: %v", err))
	}

	return nil
}

// gets a users position in a queue
func GetUserPosition(userId int, queueId int) (int, *DbError) {
	db := connect()
	defer db.Close()

	var membership QueueMembership
	err := db.QueryRow("SELECT * FROM Queue_Memberships WHERE user_id = $1 AND queue_id = $2 ",
		userId, queueId).Scan(&membership.ID, &membership.Queue, &membership.Member, &membership.Position)

	if err != nil {
		if err == sql.ErrNoRows {
			return 0, NewDbError(http.StatusNotFound, "user not found in the queue")
		}
		return 0, NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to get user position: %v", err))
	}

	return membership.Position, nil
}

func RemoveUserFromQueue(userId int, queueId int) *DbError {
	_, dbErr := withTransaction(func(tx *sql.Tx) (interface{}, *DbError) {
		return nil, removeUserFromQueue(tx, userId, queueId)
	})

	return dbErr
}

// removes user from a queue and updates position of users after them
// SHOULD BE CALLED WITHIN A TRANSACTION
func removeUserFromQueue(tx *sql.Tx, userId int, queueId int) *DbError {
	// find the position of the user in the queue
	var position int
	err = tx.QueryRow("SELECT position FROM Queue_Memberships WHERE user_id = $1 AND queue_id = $2", userId, queueId).Scan(&position)
	if err != nil {
		defer rollbackTransaction(tx)
		if err == sql.ErrNoRows {
			return NewDbError(http.StatusBadRequest, "user not found in the queue")
		}
		return NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to find user's position: %v", err))
	}

	// delete the user from the queue
	_, err = tx.Exec("DELETE FROM Queue_Memberships WHERE user_id = $1 AND queue_id = $2", userId, queueId)
	if err != nil {
		defer rollbackTransaction(tx)
		return NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to delete user from queue: %v", err))
	}

	// update positions of users after the deleted user
	_, err = tx.Exec("UPDATE Queue_Memberships SET position = position - 1 WHERE queue_id = $1 AND position > $2", queueId, position)
	if err != nil {
		defer rollbackTransaction(tx)
		return NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to update positions: %v", err))
	}

	// find the size of the queue
	var queueSize int
	err = tx.QueryRow("SELECT size FROM Queues WHERE id = $1", queueId).Scan(&queueSize)
	if err != nil {
		defer rollbackTransaction(tx)
		if err == sql.ErrNoRows {
			return NewDbError(http.StatusBadRequest, "queue not found")
		}
		return NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to find queue's size: %v", err))
	}

	// update queue size
	_, err = tx.Exec("UPDATE Queues SET size = $1 WHERE id = $2", queueSize-1, queueId)
	if err != nil {
		defer rollbackTransaction(tx)
		return NewDbError(http.StatusInternalServerError, fmt.Sprintf("failed to update queue's size: %v", err))
	}

	return nil
}
