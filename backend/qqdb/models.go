package qqdb

// user entity representation in database
type User struct {
	ID       int    `json:"id"`
	Email    string `json:"email"`
	Username string `json:"username"`
	Password string `json:"password"`
	Phone    string `json:"phone:"`
	Picture  []byte `json:"picture"`
}

// queue entity representation in database
type DiskQueue struct {
	ID    int    `json:"id"`
	Owner int    `json:"owner"`
	Name  string `json:"name"`
	Size  int    `json:"size"`
}

// queue membership representation in database
type QueueMembership struct {
	ID       int `json:"id"`
	Queue    int `json:"queue"`
	Member   int `json:"member"`
	Position int `json:"position"`
}

// in memory representation of a queue
type MemQueue struct {
	ID    int    `json:"id"`
	Owner string `json:"owner"`
	Name  string `json:"name"`
	Users []User `json:"users"`
}

// another in memory representation of a queue
type PosQueue struct {
	ID       int    `json:"id"`
	Owner    string `json:"owner"`
	Name     string `json:"name"`
	Size     int    `json:"size"`
	Position int    `json:"position"`
}

// error returned by database functions
type DbError struct {
	StatusCode int
	Message    string
}

// error method makes DbError implement the error interface
func (e *DbError) Error() string {
	return e.Message
}

// error method makes DbError implement the error interface
func (e *DbError) Status() int {
	return e.StatusCode
}

// NewAPIError creates a new DbError
func NewDbError(statusCode int, message string) *DbError {
	return &DbError{
		StatusCode: statusCode,
		Message:    message,
	}
}
