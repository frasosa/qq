package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"qqapi/qqdb"
	"strconv"

	"github.com/common-nighthawk/go-figure"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"golang.org/x/crypto/bcrypt"
)

// middleware for global error handling
func ErrorHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				fmt.Println(err)
				c.JSON(http.StatusInternalServerError, gin.H{"error": "Internal Server Error"})
			}
		}()
		c.Next()
	}
}

func main() {
	myFigure := figure.NewFigure("qqapi", "", true)
	myFigure.Print()

	// load the .env file
	godotenv.Load()
	// err := godotenv.Load()
	// if err != nil {
	// 	log.Fatalf("Error loading .env file")
	// }
	
	// init database connection and database tables if needed
	qqdb.Init()

	// create router with error handling middleware for panics
	// gin.SetMode(gin.ReleaseMode)
	router := gin.Default()
	router.Use(ErrorHandler())

	/* UNPROTECTED ROUTES */
	router.POST("/users", signUp)
	router.GET("/users", signIn)

	router.GET("/queues", getQueues)
	router.GET("/queues/:id", getQueue)

	// create group and auth middleware
	protected := router.Group("/")
	protected.Use(AuthMiddleware())

	/* PROTECTED ROUTES */
	protected.DELETE("/users", deleteUser)

	protected.POST("/queues", postQueue)
	protected.DELETE("/queues/:id", deleteQueue)

	protected.GET("/queues/users/waiting", waitingQueues)
	protected.GET("/queues/users/owned", ownedQueues)

	protected.POST("/queues/:id/users", appendToQueue)
	protected.GET("/queues/:id/users", positionInQueue)
	protected.DELETE("/queues/:id/users", removeFromQueue)

	router.Run(":" + os.Getenv("PORT"))
}

func handleDbError(dbErr *qqdb.DbError, c *gin.Context) bool {
	if dbErr != nil {
		if dbErr.Status() != http.StatusInternalServerError {
			c.JSON(dbErr.Status(), gin.H{"error": dbErr.Error()})
			return true
		} else {
			log.Panic(dbErr)
			return true
		}
	}
	return false
}

/* UNPROTECTED ROUTES*/

// sign up logic
func signUp(c *gin.Context) {
	var user qqdb.User
	var err error

	// call BindJSON to bind the received JSON to a in memory struct
	if err = c.BindJSON(&user); err != nil {
		// return specific error response for invalid JSON payload
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid JSON payload"})
		return
	}

	if user.Email == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Email is empty"})
		return
	} else if user.Username == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Username is empty"})
		return
	} else if user.Password == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Password is empty"})
		return
	}

	// hash and salt password
	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), 10)
	if err != nil {
		log.Panic(err.Error())
	}
	user.Password = string(hash)

	// Add the new user
	var dbErr *qqdb.DbError
	user.ID, dbErr = qqdb.CreateUser(user)
	if handleDbError(dbErr, c) {
		return
	}

	// generate auth token and send it back
	token, err := generateToken(strconv.FormatInt(int64(user.ID), 10))
	if err != nil {
		log.Panic(err)
		return
	}
	c.IndentedJSON(http.StatusOK, gin.H{"user": user.ID, "token": token})
}

// sign in
func signIn(c *gin.Context) {
	var infoEntered qqdb.User
	var err error

	// call BindJSON to bind the received JSON to a in memory struct
	if err = c.BindJSON(&infoEntered); err != nil {
		// return specific error response for invalid JSON payload
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid JSON payload"})
		return
	}

	if infoEntered.Email == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Email is empty"})
		return
	} else if infoEntered.Password == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Password is empty"})
		return
	}

	// get the users info
	user, dbErr := qqdb.LookupUserByEmail(infoEntered.Email)
	if handleDbError(dbErr, c) {
		return
	}

	// check if the password matches
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(infoEntered.Password))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Incorrect password"})
		return
	}

	// generate auth token and send it back
	token, err := generateToken(strconv.FormatInt(int64(user.ID), 10))
	if err != nil {
		log.Panic(err)
		return
	}
	c.IndentedJSON(http.StatusOK, gin.H{"user": user.ID, "token": token})
}

// show all queues
func getQueues(c *gin.Context) {
	queues, dbErr := qqdb.GetAllQueues()
	if handleDbError(dbErr, c) {
		return
	}
	c.JSON(http.StatusOK, queues)
}

// get specific queue with people in it
func getQueue(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Panic(err)
	}

	queues, dbErr := qqdb.GetQueue(id)
	if handleDbError(dbErr, c) {
		return
	}

	c.JSON(http.StatusOK, queues)
}

/* PROTECTED ROUTES */

// delete user
func deleteUser(c *gin.Context) {
	userId, err := getUserId(c)
	if err != nil {
		log.Panic(err)
	}

	// delete the user from the databse
	dbErr := qqdb.DeleteUser(userId)
	if handleDbError(dbErr, c) {
		return
	}

	c.Status(http.StatusOK)

}

// create queue
func postQueue(c *gin.Context) {
	userId, err := getUserId(c)
	if err != nil {
		log.Panic(err)
	}

	// create new queue
	var queue qqdb.DiskQueue
	queue.Owner = userId
	if err = c.BindJSON(&queue); err != nil {
		log.Panic(err)
	}

	// insert new queue in database
	var dbErr *qqdb.DbError
	queue.ID, dbErr = qqdb.CreateQueue(queue)
	if handleDbError(dbErr, c) {
		return
	}

	c.JSON(http.StatusCreated, queue)
}

// delete a queue
func deleteQueue(c *gin.Context) {
	// get id param
	queueId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Panic(err)
	}

	// delete the queue
	dbErr := qqdb.DeleteQueue(queueId)
	if handleDbError(dbErr, c) {
		return
	}

	c.Status(http.StatusOK)
}

// get all queues this user is in
func waitingQueues(c *gin.Context) {
	userId, err := getUserId(c)
	if err != nil {
		log.Panic(err)
	}

	queues, dbErr := qqdb.WaitingQueues(userId)
	if handleDbError(dbErr, c) {
		return
	}

	c.JSON(http.StatusOK, queues)
}

// get all queues this user is the owner of
func ownedQueues(c *gin.Context) {
	userId, err := getUserId(c)
	if err != nil {
		log.Panic(err)
	}

	queues, dbErr := qqdb.OwnedQueues(userId)
	if handleDbError(dbErr, c) {
		return
	}

	c.JSON(http.StatusOK, queues)
}

// add user to queue
func appendToQueue(c *gin.Context) {
	userId, err := getUserId(c)
	if err != nil {
		log.Panic(err)
	}

	queueId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Panic(err)
	}

	dbErr := qqdb.AppendUserToQueue(userId, queueId)
	if handleDbError(dbErr, c) {
		return
	}

	c.Status(http.StatusCreated)
}

// get users position in a queue
func positionInQueue(c *gin.Context) {
	userId, err := getUserId(c)
	if err != nil {
		log.Panic(err)
	}

	queueId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Panic(err)
	}

	pos, dbErr := qqdb.GetUserPosition(userId, queueId)
	if handleDbError(dbErr, c) {
		return
	}

	c.JSON(http.StatusOK, gin.H{"Position": pos})
}

// remove user from queue
// get users position in a queue
func removeFromQueue(c *gin.Context) {
	userId, err := getUserId(c)
	if err != nil {
		log.Panic(err)
	}

	queueId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Panic(err)
	}

	dbErr := qqdb.RemoveUserFromQueue(userId, queueId)
	if handleDbError(dbErr, c) {
		return
	}

	c.Status(http.StatusOK)
}
