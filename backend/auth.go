/*
Token Authentication using JWT
*/
package main

import (
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

// secret used to sign tokens
var jwtSecret = []byte(os.Getenv("SIGN"))

// jwt claim encapsulated standard claim and adds a string for the user id
type CustomClaims struct {
	UserId string
	jwt.StandardClaims
}

// creates and returns a token
func generateToken(userId string) (string, error) {
	// create token claims
	claims := CustomClaims{
		userId,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
		},
	}

	// make token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// sign token
	tokenString, err := token.SignedString(jwtSecret)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

// parses a token and confirms if it is valid
func parseToken(tokenString string) (string, error) {
	// Parse the token with custom claims
	token, err := jwt.ParseWithClaims(tokenString, &CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return jwtSecret, nil
	})

	// Handle parsing errors
	if err != nil {
		fmt.Print(err.Error())
		return "", fmt.Errorf("parsing error")
	}

	// Check token validity and type assertion
	if claims, ok := token.Claims.(*CustomClaims); ok && token.Valid {
		return claims.UserId, nil
	}

	return "", fmt.Errorf("invalid token")
}

// getter for userId inserted into request header by auth middleware
func getUserId(c *gin.Context) (int, error) {
	// make sure the header exists
	userId, exists := c.Get("userId")
	if !exists {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "User ID not found in context"})
		c.Abort()
	}

	// convert it to an integer
	userIdInt, err := strconv.Atoi(userId.(string))
	return userIdInt, err
}

// authentication middleware for protected routes
func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		// get header
		tokenString := c.GetHeader("Authorization")
		if tokenString == "" {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "No authorization header provided"})
			c.Abort()
			return
		}

		// remove "Bearer " prefix
		if len(tokenString) > 7 && tokenString[:7] == "Bearer " {
			tokenString = tokenString[7:]
		} else {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid authentication type in header"})
			c.Abort()
			return
		}

		// parse and validate token
		userId, err := parseToken(tokenString)
		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid token"})
			c.Abort()
			return
		}

		// set header for userid so that the request knows who is making the request
		c.Set("userId", userId)
		c.Next()
	}
}
